const { Post } = require ("../models")

module.exports = {
    getAllPost() {
        const posts = Post.findAll();
        return posts;
    },
}
